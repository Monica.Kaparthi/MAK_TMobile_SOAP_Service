package com.mak.soap.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="promotions")
@XmlAccessorType(XmlAccessType.FIELD)
public class Promotions {
	
	@XmlElement
	private List<Deal> deal;

	public List<Deal> getDeal() {
		return deal;
	}

	public void setDeal(List<Deal> deal) {
		this.deal = deal;
	}
	
	

}
