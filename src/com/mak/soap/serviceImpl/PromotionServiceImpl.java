package com.mak.soap.serviceImpl;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.mak.soap.conversion.JAXBConvert;
import com.mak.soap.entity.Deal;
import com.mak.soap.service.PromotionsService;

@WebService
public class PromotionServiceImpl implements PromotionsService {
private JAXBConvert jaxbConvert = new JAXBConvert();
	@Override
	@WebMethod
	public List<Deal> getAllDeals() {
		
		List<Deal> dealsList = jaxbConvert.convertJAXBToJava();
		
		return dealsList;
	}

	
}
