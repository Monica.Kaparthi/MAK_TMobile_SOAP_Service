package com.mak.soap.conversion;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.mak.soap.entity.Deal;
import com.mak.soap.entity.Promotions;

public class JAXBXMLHandler {

	public static List<Deal> unmarshal(File importFile) throws IOException, JAXBException {

		Promotions promotions = new Promotions();
		JAXBContext context = JAXBContext.newInstance(Promotions.class);
		Unmarshaller unmarshall = context.createUnmarshaller();
		promotions = (Promotions) unmarshall.unmarshal(importFile);
       
		return promotions.getDeal();
	}

}
