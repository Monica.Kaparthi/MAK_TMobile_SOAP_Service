package com.mak.soap.conversion;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.mak.soap.entity.Deal;

public class JAXBConvert {

	public List<Deal> convertJAXBToJava(){
		List<Deal> unmarshalledList = new ArrayList<>();
		try {
			unmarshalledList = JAXBXMLHandler.unmarshal(new File("Promotions.xml"));
			
		} catch (IOException | JAXBException e) {
			
			e.printStackTrace();
		}
		return unmarshalledList;
		
	}
}
